<?php


namespace App\Http\Controllers\Api;


use App\Actions\SearchTownhouseAction;
use App\Http\Resources\TownhouseResource;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class TownHouseController extends \App\Http\Controllers\Controller
{
    public function searchTownhouse(Request $request, SearchTownhouseAction $action)
    {
        /** @var Collection $townhouses */
        $townhouses = $action->execute($request);

        return TownhouseResource::collection($townhouses);
    }

}

<?php


namespace App\Actions;


use App\Models\Townhouse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class SearchTownhouseAction
{
    public function execute(Request $request): Collection
    {
        $filters = json_decode($request->get('filters'), true);
        $query = Townhouse::query()->select('townhouses.*');

        if(!empty($filters['name'])) {
            $name = $filters['name'];
            $query->where('name', 'like', '%' . $name . '%');
        }

        if(!empty($filters['min_price']) && !empty($filters['max_price'])) {
            (int) $min_price = $filters['min_price'];
            (int)$max_price = $filters['max_price'];
            $query->whereBetween('price', [$min_price, $max_price]);
        }

        if(!empty($filters['min_price']) && empty($filters['max_price'])) {
            (int)$min_price = $filters['min_price'];
            $query->where('price', '>', $min_price);
        }

        if(empty($filters['min_price']) && !empty($filters['max_price'])) {
            (int)$max_price = $filters['max_price'];
            $query->where('price', '<', $max_price);
        }

        if(!empty($filters['bedrooms'])) {
            $bedrooms = $filters['bedrooms'];
            $query->whereIn('bedrooms', $bedrooms);
        }

        if(!empty($filters['bathrooms'])) {
            $bathrooms = $filters['bathrooms'];
            $query->whereIn('bathrooms', $bathrooms);
        }

        if(!empty($filters['garages'])) {
            $garages = $filters['garages'];
            $query->whereIn('garages', $garages);
        }

        return $query->get();
    }

}
